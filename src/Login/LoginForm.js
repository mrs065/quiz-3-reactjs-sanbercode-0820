import React, {useContext, useState, useEffect} from "react"
import axios from "axios"
import {LoginContext} from "./LoginContext"

const LoginForm = () => {
    const [acc, setAcc] = useContext(LoginContext)
    const [input, setInput] = useState({
        username: "",
        password: ""
    })

    const handleChangeUsername = (event) => {
        var value = event.target.value
        setInput({...input, username: value})
    }

    const handleChangePassword = (event) => {
        var value = event.target.value
        setInput({...input, password: value})
    }

    const handleSubmit = (event) => {
        event.preventDefault()
    }

    return(
        <>
            <h1>Login</h1>
            <br/>
            <form onSubmit={handleSubmit}>
                <label style={{float: "left"}}><b>Username</b></label>
                <input style={{float: "right"}} type='text' name='title' value={input.username} onChange={handleChangeUsername} />
                <br />
                <br />
                <label style={{float: "left"}}><b>Password</b></label>
                <input style={{float: "right"}} type='text' name='title' value={input.password} onChange={handleChangePassword} />
                <br />
                <br />
                <div style={{width: "100%", paddingBottom: "20px"}}>
                    <button style={{float: "center"}}>Submit</button>
                </div>
            </form>
        </>
    )
}

export default LoginForm