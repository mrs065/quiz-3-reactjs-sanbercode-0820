import React, { useState, createContext } from "react";

export const LoginContext = createContext();

export const LoginProvider = props => {
    const [acc, setAcc] = useState({
      username: "",
      password: ""
    });
  
    return (
      <LoginContext.Provider value={[acc, setAcc]}>
        {props.children}
      </LoginContext.Provider>
    );
  };