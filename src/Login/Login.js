// import React from "react"
// import {LoginProvider} from "./LoginContext"
// import LoginForm from "./LoginForm"

// const Login = () => {
//     return(
//         <section>
//             <LoginProvider>
//                 <LoginForm />
//             </LoginProvider>
//         </section>
//     )
// }

// export default Login

import React, { useState } from 'react';
 
function Login(props) {
  const username = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
 
  // handle button click of login form
    const handleLogin = () => {
        if(username=='admin' && password=='admin'){
            props.history.push('/movie-list-editor');
        }
    }
 
  return (
      <section>
        <div>
            Login<br /><br />
            <div>
                Username<br />
                <input type="text" {...username} autoComplete="new-password" />
            </div>
            <div style={{ marginTop: 10 }}>
                Password<br />
                <input type="password" {...password} autoComplete="new-password" />
            </div>
            {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
            <input type="button" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading} /><br />
        </div>
      </section>
    
  );
}
 
const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);
 
  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}
 
export default Login;