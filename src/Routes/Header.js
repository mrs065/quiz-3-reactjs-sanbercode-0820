import React from "react";
import Routes from "./Routes"

export default function App(){
    return(
        <header>
            <img id='logo' src={require('../Public/img/logo.png')} width='200px' />
            <Routes />
        </header>
    )
}