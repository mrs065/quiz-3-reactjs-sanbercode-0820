import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Nav from "./Nav.js";
import About from '../About/About'
import Home from '../Home/Home'
import Movie from '../MovieEditor/Movie'
import Login from '../Login/Login'
import PrivateRoute from '../Utils/PrivateRoute';
import PublicRoute from '../Utils/PublicRoute';

export default function App(){
    return (
        <Router>
            <header>
                <div style={{width: '900px', margin: 'auto'}}>
                    <img id='logo' src={require('../Public/img/logo.png')} width='200px' />
                    <Nav /> 
                </div>
            </header>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/about" component={About} />
                <PrivateRoute exact path="/movie-list-editor" component={Movie} />
                {/* <Route exact path="/movie-list-editor" component={Movie} /> */}
                <PublicRoute exact path="/login" component={Login} />
                {/* <Route exact path="/logout" component={} /> */}
            </Switch>
        </Router>
        
        
    )
}