import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default function Nav(){
    return(
        <nav>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/about">About</Link>
                </li>
                <li>
                    <Link to="/movie-list-editor">Movie List Editor</Link>
                </li>
                <li>
                    <Link to="/login">Login</Link>
                </li>
                {/* <li>
                    <Link to="/logout">Logout</Link>
                </li> */}
            </ul>
        </nav>
    )
}