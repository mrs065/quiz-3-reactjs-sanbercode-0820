import React, {useState, useEffect, Component} from 'react';
import axios from 'axios';

class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            dataFilm: null,
            setDataFilm: null,
            isTop: true
        }
    }

    componentDidMount(){
        if(this.state.dataFilm === null){
            axios.get("http://backendexample.sanbercloud.com/api/movies")
            .then(res => {
                const dataFilm = res.data
                this.setState({dataFilm})
                console.log(res.data)
            })
        }
        document.addEventListener('scroll', () => {
            const isTop = window.scrollY < 100;
            if(isTop!== this.state.isTop) {
                this.setState({isTop})
            }
        });
    }

    render(){
        return(
            <section>
             <h1 style={{textAlign: "center"}}>Data Film Film Terbaik</h1>
             <br />
             {
                this.state.dataFilm !== null && this.state.dataFilm.map((item) => {
                    return(
                        <div className='article'>
                            <table>
                                <tr>
                                    <td colSpan='2' style={{textAlign: "left"}}><h2>{item.title}</h2></td>
                                </tr>
                                <tr>
                                    <td id='image'><img src={item.image_url} /></td>
                                    <td style={{textAlign:'left', verticalAlign:'top'}}>
                                        <b>Rating: {item.rating}</b><br />
                                        <b>Duration: {item.duration}</b><br/>
                                        <b>Genre: {item.genre}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{textAlign:'left'}} colSpan='2'>{item.description}</td>
                                </tr>
                            </table>
                        </div>
                    )
                })
            }
        </section>
        )
    }
}
export default Home