import React, {useContext, useState, useEffect} from "react"
import axios from "axios"
import {MovieContext} from "./MovieContext"

const MovieForm = () => {
    const [movie, setMovie] = useContext(MovieContext)
    const [input, setInput] = useState({
        title: '',
        description: '',
        year: '',
        duration: '',
        genre: '',
        rating: '',
        image_url: ''
    })

    useEffect(()=> {
        if(movie.statusForm === 'changeToEdit'){
            let movieData = movie.lists.find(x => x.id === movie.selectedId)
            setInput({
                title: movieData.title,
                description: movieData.description,
                year: movieData.year,
                duration: movieData.duration,
                genre: movieData.genre,
                rating: movieData.rating,
                image_url: movieData.image_url
            })
            setMovie({...movie, statusForm: "edit"})
        }
    }, [movie, setMovie])

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch(typeOfInput) {
            case "title":{
                setInput({...input, title: event.target.value});
                break
            }
            case "description":{
                setInput({...input, description: event.target.value});
                break
            }
            case "year":{
                setInput({...input, year: event.target.value});
                break
            }
            case "duration":{
                setInput({...input, duration: event.target.value});
                break
            }
            case "genre":{
                setInput({...input, genre: event.target.value});
                break
            }
            case "rating":{
                setInput({...input, rating: event.target.value});
                break
            }
            case "image_url":{
                setInput({...input, image_url: event.target.value});
                break
            }
            default: {break;}
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        if(movie.statusForm === "create"){
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title: input.title, description: input.description, year: input.year, duration: input.duration, genre: input.genre, rating: input.rating, image_url: input.image_url})
            .then(res => {
                setMovie(
                    {statusForm: "create", selectedId: 0,
                        lists: [
                            ...movie.lists,
                            {
                                id: res.data.id,
                                title: input.title,
                                description: input.description,
                                year: input.year,
                                duration: input.duration,
                                genre: input.genre,
                                rating: input.rating,
                                image_url: input.image_url
                            }
                        ]
                    }
                )
            })
        } else if (movie.statusForm === "edit"){
            axios.put(`http://backendexample.sanbercloud.com/api/movies/${movie.selectedId}`, {title: input.title, description: input.description, year: input.year, duration: input.duration, genre: input.genre, rating: input.rating, image_url: input.image_url})
            .then(() => {
                let movieData = movie.lists.find(el => el.id === movie.selectedId)
                movieData.title = input.title
                movieData.description = input.description
                movieData.year = input.year
                movieData.duration = input.duration
                movieData.genre = input.genre
                movieData.rating = input.rating
                movieData.image_url = input.image_url
                setMovie({statusForm: "create", selectedId: 0, lists: [...movie.lists]})
            })
        }
        setInput({
            title: "",
            description: "",
            year: "",
            duration: "",
            genre: "",
            rating: "",
            image_url: ""
        })
    }
    
    return(
        <div style={{width: "50%", margin: "0 auto", display: "block"}}>
            <div style={{border: "1px solid #aaa", padding: "20px"}}>
            <h1>Movie Form</h1>
            <br />
            <form onSubmit={handleSubmit}>
                <label style={{float: "left"}}><b>Title</b></label>
                <input style={{float: "right"}} type='text' name='title' value={input.title} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Description</label>
                <textarea style={{float: "right"}} rows="2" name='description' value={input.description} onChange={handleChange}></textarea>
                <br />
                <br />
                <br />
                <label style={{float: "left"}}>Year</label>
                <input style={{float: "right"}} type='text' name='year' value={input.year} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Duration</label>
                <input style={{float: "right"}} type='text' name='duration' value={input.duration} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Genre</label>
                <input style={{float: "right"}} type='text' name='genre' value={input.genre} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Rating</label>
                <input style={{float: "right"}} type='text' name='rating' value={input.rating} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Image URL</label>
                <textarea style={{float: "right"}} rows="3" name='image_url' value={input.image_url} onChange={handleChange}></textarea>
                <br />
                <br />
                <br />
                <br />
                <div style={{width: "100%", paddingBottom: "20px"}}>
                    <button style={{float: "center"}}>Submit</button>
                </div>
            </form>
            </div>
        </div>
    )
}

export default MovieForm