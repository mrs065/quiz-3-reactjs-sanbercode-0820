import React, {useContext, useEffect, useState} from "react"
import axios from "axios"
import {MovieContext} from "./MovieContext"

const MovieList = () => {
    const [movie, setMovie] = useContext(MovieContext)
    const [input, setInput] = useState({titleMovie: ''})

    useEffect(() => {
        if(movie.lists === null){
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setMovie({
                    ...movie,
                    lists: res.data.map(el =>{
                        return{
                            id: el.id,
                            title: el.title,
                            description: el.description,
                            year: el.year,
                            duration: el.duration,
                            genre: el.genre,
                            rating: el.rating,
                            image_url: el.image_url
                        }
                    })
                })
            })
        }
    }, [setMovie, movie])

    const editMovie = (event) => {
        let idMovie = parseInt(event.target.value)
        setMovie({...movie, selectedId: idMovie, statusForm: 'changeToEdit'})
    }

    const deleteMovie = (event) => {
        let idMovie = parseInt(event.target.value)
        let newMovieList = movie.lists.filter(el => el.id !== idMovie)
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
        .then(res=> {
            console.log(res)
        })
        setMovie({...movie, lists: [...newMovieList]})
    }

    const filterMovie = (event) => {
        let titleMovie = event.target.value
        axios.get(`http://backendexample.sanbercloud.com/api/movies/`)
        .then(res => {
            let result = movie.lists.filter(el => el.title == titleMovie)
            setMovie(result)
        })
    }

    const handleChangeSearch = (event) => {
        var value = event.target.value
        setInput({titleMovie: value})
    }

    return(
        <>
            <input type='text' value={input.titleMovie} onChange={handleChangeSearch} name='search'/>
            &nbsp;
            <button onClick={filterMovie}>Search</button>
            <h1>Daftar Film</h1>
            <br />
            <table style={{borderCollapse: "collapse", margin: '0 auto'}}>
                <thead>
                    <tr style={{borderBottom: '1px solid #dddddd'}}>
                        <th>No</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Duration</th>
                        <th>Genre</th>
                        <th>Rating</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        movie.lists !== null && movie.lists.map((item, index) => {
                            return(
                                <tr key={index} style={{borderBottom: '1px solid #dddddd'}}>
                                    <td>{index+1}</td>
                                    <td>{item.title}</td>
                                    <td id='truncate'>{item.description}</td>
                                    <td>{item.year}</td>
                                    <td>{item.duration}</td>
                                    <td>{item.genre}</td>
                                    <td>{item.rating}</td>
                                    <td>
                                        <button onClick={editMovie} value={item.id}>Edit</button>
                                        &nbsp;
                                        <button onClick={deleteMovie} value={item.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </>
    )
}

export default MovieList