import React from 'react';
// import logo from './logo.svg';
import './App.css';
import './Public/css/style.css';
import Header from './Routes/Header'
import Home from './Home/Home'
import Movie from './MovieEditor/Movie'
import Routes from './Routes/Routes'

function App() {
  return (
    <div className="App">
      <Routes />
      {/* <Header /> */}
      {/* <Home />
      <Movie /> */}
    </div>
  );
}

export default App;
