import React from 'react';

let dataDiri = [
    {
        nama: "Mega Resty Sudigdo", 
        email: "megarestys@gmail.com", 
        os: "Windows", 
        akunGitlab: "@mrs065", 
        akunTelegram: "@mega_resty"
    }
]

class IsiData extends React.Component{
    render(){
        return(
            <ol>
                <li><strong style={{width: '100px'}}>Nama:</strong> {this.props.item.nama}</li>
                <li><strong style={{width: '100px'}}>Email:</strong> {this.props.item.email}</li>
                <li><strong style={{width: '100px'}}>Sistem Operasi yang digunakan:</strong> {this.props.item.os}</li>
                <li><strong style={{width: '100px'}}>Akun Gitlab:</strong> {this.props.item.akunGitlab}</li>
                <li><strong style={{width: '100px'}}>Akun Telegram:</strong> {this.props.item.akunTelegram}</li>
            </ol>
        )
    }
}

function GrupData(){
    return(
        <>
            {
                dataDiri.map((el)=>{
                    return(
                        <IsiData item={el}/>
                    )
                })
            }
        </>
    )
    
}

function About(){
    return(
        <section>
            <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
            <br/>
            <GrupData />
        </section>
        
    )
}

export default About